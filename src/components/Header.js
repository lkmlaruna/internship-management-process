import React, { Component } from 'react';
import '../styles/header.css';
import logo from '../resources/7-512.png'

class Header extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img className="App-logo" src={logo}/>
                    <h1 className="App-title">Internship Program Process</h1>
                </header>
            </div>
        );
    }
}

export default Header;
