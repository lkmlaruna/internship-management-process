import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../styles/navStyles.css';


class NavBar extends Component {
    handleClick = (navigation) => {
        if(navigation == 'waiting') {
            window.location.href = `http://localhost:3000`;
        } else if(navigation == 'history') {
            window.location.href = `http://localhost:3000/history`;
        } else if(navigation == 'admitted') {
            window.location.href = `http://localhost:3000/admittedList`;
        } else {
            window.location.href = `http://localhost:3000/dischargedList`;
        }

    }

    render() {
        return (
            <div className='vertical-menu'>
                <a href="#">Link 1</a>
                <a href="#">Link 2</a>
                <a href="#">Link 3</a>
                <a href="#">Link 4</a>
            </div>
        );
    }
}

export default NavBar;
