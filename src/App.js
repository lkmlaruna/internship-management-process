import React, { Component } from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import Navigation from './components/NavBar'
import Header from './components/Header'
import './styles/FlexLayout.css'

class App extends Component {
    constructor(props) {
        super(props)
    }

  render() {
        const style = {
            padding: 0,
        }
    return (
        <div>
            <div className="header">
                <Header/>
            </div>
            <div className="row">
                <div className="side">
                    <div className="vertical-menu">
                        <a href="#">Form I-1</a>
                        <a href="#">Form I-3</a>
                        <a href="#">Form I-5</a>
                        <a href="#">Form I-7</a>
                    </div>
                </div>
                <div className="main">
                    <h2>TITLE HEADING</h2>
                    <h5>Title description, Dec 7, 2017</h5>
                    <div className="fakeimg" style={{height:200}}>Image</div>
                    <p>Some text..</p>
                    <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do
                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco.</p>
                    <br/>
                        <h2>TITLE HEADING</h2>
                        <h5>Title description, Sep 2, 2017</h5>
                        <div className="fakeimg" style={{height:200}}>Image</div>
                        <p>Some text..</p>
                        <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco.</p>
                </div>
            </div>
        </div>
    );
  }
}

export default App;
