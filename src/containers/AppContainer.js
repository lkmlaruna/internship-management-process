import React, { Component } from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import Route from 'react-router-dom/Route';
import App from '../App';
import Header from '../components/Header';

class AppContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <div>
                    <Route path="/" component={App}/>
                </div>
            </Router>
        );
    }
}

export default AppContainer;